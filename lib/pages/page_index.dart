import 'package:flutter/material.dart';

class PageIndex extends StatelessWidget {
  const PageIndex({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffBCCFFF),
      appBar: AppBar( backgroundColor: Colors.green,
        title: const Text(
            '내 사랑 라인이',
        style: TextStyle(color:Colors.white70 ),),
        centerTitle: true,
      ),drawer: Drawer(),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(margin: EdgeInsets.all(15),
              child:
            Image.asset('assets/dog1.png',
                width: 300,
                height: 200,
                fit: BoxFit.fill),),
            Container( margin: EdgeInsets.all(10),
              width: 300,
              child:Column(
                children: [
                   Container( alignment: Alignment.centerLeft,
                     margin: EdgeInsets.only(top: 5),
                     child:
                  Text('라인이를 소개합니다.' ,
                  style: TextStyle(color: Colors.black54,fontWeight: FontWeight.bold)),),
                  Container(alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(top: 5),
                    child:
                  Text("라인이는 유기션 출신으로",
                      style: TextStyle(color: Colors.grey)),),
                  Container( alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(top: 5),
                    child:
                  Text("길에서 발견되어 구조됐습니다.",
                      style: TextStyle(color: Colors.black54)),)
                ],
              ),
            ),
            Container( margin: EdgeInsets.all(20),
              child: Column(
                children: [
                Row( mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container( margin: EdgeInsets.all(10),
                      child:
                    Image.asset('assets/dog2.png',
                        width: 130,
                        height: 110,
                        fit: BoxFit.fill
                    ),),
                    Container( margin: EdgeInsets.all(10),
                      child:
                    Image.asset('assets/dog3.png',
                        width: 130,
                        height: 110,
                        fit: BoxFit.fill
                    ),),
                  ],
                ),
                  Row( mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container( margin: EdgeInsets.all(10),
                        child:
                      Image.asset('assets/cat2.png',
                          width: 130,
                          height: 110,
                          fit: BoxFit.fill
                      ),),
                      Container( margin: EdgeInsets.all(10),
                        child:
                      Image.asset('assets/cat1.png',
                          width: 130,
                          height: 110,
                          fit: BoxFit.fill
                      ),),
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
